import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContentProfilePage());
}

class ContentProfilePage extends StatefulWidget {
  const ContentProfilePage({super.key});

  @override
  State<ContentProfilePage> createState() => _ContentProfilePageState();
}

class _ContentProfilePageState extends State<ContentProfilePage> {
  var currentTheme = APP_THEME.DARK;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: BuildAppbarWidget(),
        body: BuildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
          backgroundColor: Colors.amber,
          elevation: 3.0,
          tooltip: "Change Theme",
          child: Icon(Icons.light_mode),
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
      title: Text("094-347xxxx"),
      subtitle: Text("mobile"),
      leading: Icon(Icons.call),
      trailing: IconButton(
        icon: Icon(
          Icons.message,
        ),
        onPressed: () {},
      ));
}

Widget otherPhoneListTile() {
  return ListTile(
    title: Text("098-845xxxx"),
    subtitle: Text("other"),
    leading: Text(""),
    trailing: IconButton(
      icon: Icon(
        Icons.message,
      ),
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    title: Text("nasan@gmail.com"),
    subtitle: Text("work"),
    leading: Icon(Icons.email),
  );
}

Widget addressListTile() {
  return ListTile(
    title: Text("9 Tesaban3 St, Klaeng"),
    subtitle: Text("home"),
    leading: Icon(Icons.location_on),
    trailing: IconButton(
      icon: Icon(
        Icons.directions,
      ),
      onPressed: () {},
    ),
  );
}

const divide = Divider(
  color: Colors.grey,
);

AppBar BuildAppbarWidget() {
  return AppBar(
    leading: IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {},
    ),
    title: Text("ContactPage"),
    actions: [
      IconButton(
        onPressed: () {
          print("Contact is starred");
        },
        icon: Icon(Icons.star),
      )
    ],
  );
}

Widget BuildBodyWidget() {
  return ListView(
    children: [
      Column(
        children: [
          Container(
            width: double.infinity,
            height: 250,
            child: Image.network(
              "https://i.pinimg.com/564x/23/f0/b2/23f0b2c03eb88a64ed5c4f83c24d339d.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Nasan Soonthondachar",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.deepOrange,
                ),
              ),
              child: profileActionItems(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Column(
              children: [
                divide,
                mobilePhoneListTile(),
                divide,
                otherPhoneListTile(),
                divide,
                emailListTile(),
                divide,
                addressListTile(),
              ],
            ),
          )
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.deepOrangeAccent,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      iconTheme: IconThemeData(color: Colors.black),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.deepOrangeAccent,
        iconTheme: IconThemeData(color: Colors.deepOrange),
      ),
      iconTheme: IconThemeData(color: Colors.deepOrange),
    );
  }
}
